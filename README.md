# Logik

This crate is a calculator for propositional logic formulas.

---

###### License
This program is released under the _GNU General Public License_. See _[COPYING](./COPYING)_ for
additional information about the license.

###### Contact
For any other question contact me at
_[fabian.moos@moosegamesdev.org](mailto:fabian.moos@moosegamesdev.org)_.
