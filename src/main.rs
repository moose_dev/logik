// Copyright (C) 2023  Fabian Moos
// This file is part of logik.
//
// logik is free software: you can redistribute it and/or modify it under the terms of the GNU
// General Public License as published by the Free Software Foundation, either version 3 of the
// License or (at your option) any later version.
//
// logik is distributed in the hope that it will be helpful, but WITHOUT ANY WARRANTY; without
// even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU
// General Public License for more details.
//
// You should have received a copy of the GNU General Public License along with logik. If not,
// see <https://www.gnu.org/licenses/>.

fn main() {
    println!("Hello, logik crate!");
}
